import os

def main():
    print('Hi LLA Team')
    
    # Loading environment variables
    DBPASSWORD = os.getenv('MYPROJECT_DBPASSWORD', '')
    print(f'the password is: {DBPASSWORD}')


if __name__ == '__main__':
    main()